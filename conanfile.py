#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Conan recipe package for cJSON library
"""
import os
import shutil
from conans import ConanFile, CMake, tools


class LibCJSONConan(ConanFile):
    """Download source, build, and create package
    """
    name            = "cJSON"
    version         = "0.0.1-2869195"
    settings         = "os", "compiler", "build_type", "arch"
    options         = {"shared": [True, False], "fPIC": [True, False]}
    default_options = "shared=False", "fPIC=True"
    homepage        = "https://github.com/DaveGamble/cJSON"
    url             = "https://gitlab.com/openrov/conan-cJSON"
    author          = "OpenROV <info@openrov.com>"
    license         = "MIT"
    description     = "Ultralightweight JSON parser in ANSI C"

    exports         = ["LICENSE.md"]
    exports_sources = "CMakeLists.txt"

    _source_subfolder   = "source_subfolder"
    _build_subfolder    = "build_subfolder"

    def source(self):
        commit = "28691956a607ccec14b1f31756c7f179d50a0d17"
        git = tools.Git( folder=self._source_subfolder )
        git.clone( self.homepage )
        git.checkout( commit )

        # Move the CMakeLists.txt into the source directory
        shutil.move( "CMakeLists.txt", self._source_subfolder )

    def configure(self):
        # Pure C library
        del self.settings.compiler.libcxx

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.configure( source_folder=self.build_folder + "/" + self._source_subfolder )
        return cmake

    def build(self):
        cmake = self._configure_cmake()
    
        # Add warnings
        cmake.definitions["CMAKE_C_FLAGS"]=     "-Wall "                    \
                                                "-Wpointer-arith "          \
                                                "-Wstrict-prototypes "      \
                                                "-Wundef "                  \
                                                "-Wcast-align "             \
                                                "-Wcast-qual "              \
                                                "-Wextra "                  \
                                                "-Wno-empty-body "          \
                                                "-Wno-unused-parameter "    \
                                                "-Wshadow "                 \
                                                "-Wwrite-strings "          \
                                                "-Wswitch-default "         \
                                                "-Wno-array-bounds "        \
                                                "-Wno-ignored-qualifiers"

        cmake.build()

    def package(self):
        self.copy("LICENSE", src=self._source_subfolder, dst="licenses", keep_path=False)
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
        self.cpp_info.libs.append("m")
